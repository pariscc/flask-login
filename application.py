import os
from flask import Flask, render_template, request, session, redirect, url_for
from forms import SignupForm, LoginForm
from models import UserModel


application = Flask(__name__)
application.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
application.secret_key = str(os.urandom(10))  # prevent CSRF
sys_port = int(os.getenv('PORT', 5000))


@application.before_first_request
def create_tables():
    db.create_all()


@application.route('/')
def index():
    return render_template('index.html')


@application.route('/about')
def about():
    return render_template('about.html')


@application.route('/signup', methods=['GET', 'POST'])
def signup():
    if 'email' in session:  # if user is already logged in -> redirect
        return redirect(url_for('homepage'))

    form = SignupForm()
    if request.method == 'POST':
        if form.validate() is False:
            return render_template('signup.html', form=form)

        new_user = UserModel(
            form.first_name.data,
            form.last_name.data,
            form.email.data,
            form.password.data
        )
        new_user.save_to_db()

        return redirect(url_for('successpage'))
    return render_template('signup.html', form=form)


@application.route('/login', methods=['GET', 'POST'])
def login():
    # To Do: use Flask-Admin
    if 'email' in session:  # if user is already logged in -> redirect
        return redirect(url_for('homepage'))

    form = LoginForm()
    if request.method == 'POST':
        if form.validate() is False:
            return render_template('login.html', form=form)

        email = form.email.data
        password = form.password.data

        user = UserModel.query.filter_by(email=email).first()
        # checks that the user exists and has a valid password
        if user is not None and user.get_password(password):
            session['email'] = form.email.data
            return redirect(url_for('homepage'))

        return redirect(url_for('login'))
    elif request.method == 'GET':
        return render_template('login.html', form=form)


@application.route('/logout')
def logout():
    session.pop('email', None)  # delete the cookie
    return redirect(url_for('index'))


@application.route('/successpage')
def successpage():
    return render_template('successpage.html')


@application.route('/homepage')
def homepage():
    if 'email' not in session:  # protect the homepage
        return redirect(url_for('login'))
    return render_template('homepage.html')


if __name__ == '__main__':
    from models import db
    db.init_app(application)
    application.run('0.0.0.0', port=sys_port, debug=True)
