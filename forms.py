from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import DataRequired, Email  # validate the form fields
from wtforms.validators import Length


class SignupForm(FlaskForm):
    first_name = StringField('First name', validators=[
        DataRequired('Enter your forname!')])

    last_name = StringField('Last name', validators=[
        DataRequired('Enter your surname!')])

    email = StringField('Email', validators=[
        DataRequired('Enter your email address!'),
        Email('Enter a valid email address!')])

    password = PasswordField('Password', validators=[
        DataRequired('Enter a password!'),
        Length(min=4, message='Password must be at least 4 characters long!')])

    submit = SubmitField('Sign Up')


class LoginForm(FlaskForm):
    email = StringField('Email', validators=[
        DataRequired('Please enter your email address!'),
        Email('Please enter a valid email address!')
    ])

    password = PasswordField('Password', validators=[
        DataRequired('Please enter your password!')
    ])

    submit = SubmitField('Sign In')
