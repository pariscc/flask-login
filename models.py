from flask_sqlalchemy import SQLAlchemy
from werkzeug import generate_password_hash, check_password_hash


db = SQLAlchemy()


class UserModel(db.Model):
    __tablename__ = 'users'

    uid = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(128))
    lastname = db.Column(db.String(128))
    email = db.Column(db.String(128), unique=True)
    passwdhash = db.Column(db.String(256))

    def __init__(self, firstname, lastname, email, password):
        self.firstname = firstname.title()
        self.lastname = lastname.title()
        self.email = email.lower()
        self.set_password(password)

    def set_password(self, password):
        self.passwdhash = generate_password_hash(password)

    def get_password(self, password):
        return check_password_hash(self.passwdhash, password)

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()
